
void HardFault_Handler(void)
{

	//TODO test out this function
	//should retrieve PC of the instruction that follows whatever caused the hardfault
	//This didn't work for me earlier bc the stack itself was broke
//	asm(
//	"movs r0, #4	\n"
//	"movs r1, lr	\n"
//	"tst r0, r1	\n"
//	"beq _MSP	\n"
//	"mrs r0, psp	\n"
//	"b _HALT	\n"
//	"_MSP:		\n"
//	"	mrs r0, msp	\n"
//	"_HALT:	\n"
//	"	ldr r1, [r0,#20]	\n"
//	//"	bkpt #0	\n"
//	);

	while (1) {
	}
}

#ifndef __NO_SYSTEM_INIT
//option to create your own C library system initialization
//Currently define __STARTUP_CLEAR_BSS when building to only use
//initialization provided by startup_ARMCM0.S that zero's the BSS section
//To get this called in between reset routine and main function,
//remove definition from makefile
void SystemInit()
{
//	TODO Some processor initializations might be a in good order.
//	currently just sticking with default main stack pointer (MSP)
//	process SP is unused..
}
#endif

//include target chip port definition library files
#include <stm32f0xx.h>
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_gpio.h>

//include target board library files
//this is junk... #include <stm32f072b_discovery.h>

//Here's where the LEDs and switch are located
//PA0 user switch
#define SWITCH	(0U)

//PC6 RED/UP
#define RED	(6U)
//PC7 DOWN/BLUE
#define BLUE	(7U)
//PC8 LEFT/YELLOW
#define YELLOW	(8U)
//PC9 RIGHT/GREEN
#define GREEN	(9U)

//RCC->AHBENR AHB clock enable register
//set bit to enable clock
#define	DMAEN	(0U)	//DMA clock
#define	DMA2EN	(1U)	//DMA2 clock
#define	SRAMEN	(2U)	//SRAM interface during sleep
#define	FLITFEN	(4U)	//Flash interface during sleep
#define	CRCEN	(6U)	//CRC clock
#define	IOPAEN	(17U)	//IO port A
#define	IOPBEN	(18U)	//IO port B
#define	IOPCEN	(19U)	//IO port C
#define	IOPDEN	(20U)	//IO port D
#define	IOPEEN	(21U)	//IO port E
#define	IOPFEN	(22U)	//IO port F
#define	TSCEN	(24U)	//touch sensing controller

//only one bit per pin so no extra shift needed
void init_io()
{
	//Need to supply clock to i/o port before anything can be done
	RCC->AHBENR |= ((1<<IOPAEN) | (1<<IOPCEN));
	
	//after reset most io registers are reset with exception of SWDIO/SWCLK for debugger
	//This makes all other pins Input, Pushpull (no effect till set as output), Slow, no pullup/down, output reg set low.
	//post reset, you only need to set the bits needed to set to desired config

	//SET LEDs as outputs Mode register is 2bits per pin, start as off
	//0b01 is output 2 bits per pin, 16 pins per port, fills entire 32bit MODER register
	//need to enable pins 9-6
	GPIOC->MODER |= ((GPIO_Mode_OUT<<(RED*2)) | (GPIO_Mode_OUT<<(BLUE*2)) | (GPIO_Mode_OUT<<(YELLOW*2)) | (GPIO_Mode_OUT<<(GREEN*2)));
	GPIOC->MODER &= ~((GPIO_Mode_OUT<<(RED*2)) & (GPIO_Mode_OUT<<(BLUE*2)) & (GPIO_Mode_OUT<<(YELLOW*2)) & (GPIO_Mode_OUT<<(GREEN*2)));
	
	//GPIOC->ODR |= (0x1U<<RED);
	//fuck it, output them all HIGH!
	//GPIOC->ODR = 0x0000AAAA;	//blue green
	//GPIOC->ODR = 0x00005555;	//red orange

	//Switch already set to input, just enable the pullup
	GPIOA->PUPDR |= (GPIO_PuPd_UP<<(SWITCH*2));

}

void main()
{
	//System is running at reset defaults
	
	//Default clock is in operation
	//Change system clock as needed
	//Initialize periphery clocks as needed
	
	//Initialize interupts
	
	//Initialize WDT, core features, etc
	
	//Initialize io, periphery, etc
	//setup LED's as outputs and turn them on
	//setup user switch as input
	init_io();
	
	//Initialize board/system
	
	uint32_t i = 0;
	
	//main infinite loop
	while(1) {

		i++;
		//to start i is zero and doesn't satisfy any checks below so all LEDs are off

		//once i reaches > 2M set output data register to i
		//This will make all LEDs turn on but not full brightness
		//they "flicker" as i iterates
		if ( i > 2000000 ) {
			//mask out all but the GPIO bits
			//a bit unfinished as we're writing to the entire GPIO register...
			GPIOC->ODR = (i & 0x0000FFFF);
		}
		
		//once i > 4M set some LEDs on solid
		if ( i > 4000000 ) {
			i = 0;
			//GPIOC->ODR = 0x0000AAAA;	//blue green
			GPIOC->ODR = 0x00005555;	//red orange
		}

		//once i > 8M reset back to 2M
		if ( i > 8000000 ) {
			i = 2000000;
		}

		//read in user switch
		if ( GPIOA->IDR & (1<<SWITCH) ) {
			//button pressed
			i = 0;	//reset the "counter"
			GPIOC->ODR = 0x00000000;	//all off

		} else {
			//button not pressed
		}


	}
}
